public class LZWBinTreeImproved
{
    public LZWBinTreeImproved()
    {
        tree=root;
    }

    public void builder(char b)                 //building the tree
    {
        if (b=='0')
        {
            if (tree.leftChild()==null)
            {
                Node next=new Node('0');
                tree.nextLeftChild(next);

                depth++;                        //calculating the depth of the tree
                if(depth>maxDepth)
                    maxDepth=depth;

                tree=root;
                depth=0;
            }
            else
            {
                tree=tree.leftChild();
                depth++;
            }
        }
        else
        {
            if (tree.rightChild()==null)
            {
                Node next=new Node('1');
                tree.nextRightChild(next);

                depth++;                        //calculating the depth of the tree
                if(depth>maxDepth)
                    maxDepth=depth;

                tree=root;
                depth=0;
            }
            else
            {
                tree=tree.rightChild();
                depth++;
            }
        }
    }

    class Node
    {
        public Node(char character)
        {
            this.character=character;            //deleted double null
        }

        public Node leftChild()
        {
            return leftZero;
        }

        public Node rightChild()
        {
            return rightOne;
        }

        public void nextLeftChild(Node n)
        {
            leftZero=n;
        }

        public void nextRightChild(Node n)
        {
            rightOne=n;
        }

        public char getCharacter()
        {
            return character;
        }

        private char character;

        private Node leftZero=null;
        private Node rightOne=null;
    }

    private Node tree=null;

    /*------------------------------------------------      tree in the file process
    public void output()
    {
        depth=0;
        output(root,new java.io.PrintWriter(System.out));
    }

    public void output(java.io.PrintWriter os)
    {
        depth=0;
        output(root,os);
    }

    -------------------------------------------------       tree file formating and writing

    public void output(Node piece, java.io.PrintWriter os)
    {
        if (piece!=null)
        {
            depth++;
            output(piece.rightChild(), os);

            for (int i=0; i<depth; ++i)
            {
                os.print("---");
            }

            os.print(piece.getCharacter());
            os.print("(");
            os.print(depth-1);
            os.println(")");
            output(piece.leftChild(), os);
            depth--;
        }
    }
    -------------------------------------------------- */

    protected Node root=new Node('/');
                                                    //double all the way
    private double meanSum,counter,varSum,maxDepth=0,depth=0,mean,var;

    public double getMean()                         //setting up the mean for calculation
    {
        depth=counter=meanSum=0;
        recursiveMean(root);

        mean=meanSum/counter;                       //no need for convert
        return mean;
    }

    public void recursiveMean(Node piece)           //calculating the mean of the tree
    {
        if(piece!=null)                             //deleted mean call
        {
            depth++;
            recursiveMean(piece.rightChild());
            recursiveMean(piece.leftChild());
            depth--;

            if(piece.rightChild()==null&&piece.leftChild()==null)
            {
                counter++;
                meanSum+=depth;
            }
        }
    }

    public double getVar()                          //setting up the var for calculation
    {
        varSum=0.0;
        depth=0;                                    //deleted counter counting

        recursiveVar(root);

        if(counter-1>0)
        {
            var=Math.sqrt(varSum/(counter-1));
        }
        else
        {
            var=Math.sqrt(varSum);
        }

        return var;
    }

    public void recursiveVar(Node piece)            //calculating the var of the tree
    {
        if(piece!=null)
        {
            depth++;
            recursiveVar(piece.rightChild());
            recursiveVar(piece.leftChild());
            depth--;
                                                    //1 counter is enough
            if(piece.rightChild()==null&&piece.leftChild()==null)
            {
                varSum+=((depth-mean)*(depth-mean));
            }
        }
    }

    public static void usage()          //error message
    {
        System.out.println("Usage: LZWBinTreeImproved in_file -o out_file");
    }

    public static void main(String args[])
    {
        if(args.length!=3)              //argumentum check
        {
            usage();

            System.exit(-1);
        }

        String inFile=args[0];          //unputfile from argumentum

        if(!"-o".equals(args[1]))       //fancy stuff
        {
            usage();
            System.exit(-1);
        }

        try                             //try inputfile and outputfile from argumentum
        {
            java.io.BufferedInputStream inputFile=
                new java.io.BufferedInputStream(
                new java.io.FileInputStream(
                new java.io.File(args[0])));

            java.io.PrintWriter outputFile=
                new java.io.PrintWriter(
                new java.io.BufferedWriter(
                new java.io.FileWriter(args[2])));

            byte[] b=new byte[1];

            LZWBinTree binTree=new LZWBinTree();

            while(inputFile.read(b)!=-1)
            {
                if(b[0]==0x0a)
                {
                    break;
                }
            }

            boolean inComment=false;

            while(inputFile.read(b)!=-1)    //improved if
            {

                if(b[0]==0x3e)          // >
                {
                    inComment=true;
                    continue;
                }

                else if(b[0]==0x0a)     // \n
                {
                    inComment=false;
                    continue;
                }
                
                else if(b[0]==0x4e)     // N
                {
                    continue;
                }

                if(inComment)
                {
                    continue;
                }

                for(int i=0;i<8;++i)
                {
                    if ((b[0]&0x80)!=0)
                    {
                        binTree.builder('1');
                    }
                    else
                    {
                        binTree.builder('0');
                    }
                    b[0]<<=1;
                }
            }

            inputFile.close();

            //binTree.output(outputFile);

            System.out.println("depth = "+binTree.maxDepth);    //print stats into terminal

            binTree.getMean();
            System.out.printf("mean = %.9f\n",binTree.mean);

            binTree.getVar();
            System.out.printf("var = %.9f\n",binTree.var);

            outputFile.println("depth = "+binTree.maxDepth);    //write stats into outputfile
            outputFile.printf("mean = %.9f\n",binTree.mean);
            outputFile.printf("var = %.9f",binTree.var);

            outputFile.close();
        }
        catch (java.io.FileNotFoundException fnfException)      //file not found exception
        {
            fnfException.printStackTrace();
        }
        catch (java.io.IOException ioException)                 //IO exception
        {
            ioException.printStackTrace();
        }
    }
}
